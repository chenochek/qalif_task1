module.exports = function markSpam (input) {
    let result = [];
    input.forEach(letter =>  {
        let messages = letter.split(":"),
            address = messages[0],
            message = messages[1];
        if(isAddressCorrect(address)) {
            message = replaceSpam(message);
            result.push(`${address}:${message}`);
        }

    });





    function isAddressCorrect(address) {
       let galaxy = address.split("/")[0],
           system = address.split("/")[1],
           planet = address.split("/")[2];
       return isCorrectName(/[A-Z]{2,8}-[0-9]{2,8}$/g ,galaxy) &&
           isCorrectName(/^[^-]((?!\-\-)[A-Z\-])*[^-]$/g ,system) &&
           isCorrectName(/^([A-Z])+?$/, planet);
    }

    function replaceSpam(message) {
        let spamPattern = /(@)([^@]*)(@)/g;
        return message.replace(spamPattern, (match, p1, p2) => `<spam>${p2}</spam>`);
    }

    function isCorrectName (pattern, name) {
        return pattern.test(name);

    }

    return result;
}